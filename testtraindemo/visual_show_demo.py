from detectron2.data.datasets import register_coco_instances
from detectron2.data import MetadataCatalog
from detectron2.data import DatasetCatalog
import random
import cv2
# 注册数据集
from detectron2.utils.visualizer import Visualizer

register_coco_instances("fruits_nuts", {}, "./data/trainval.json", "./data/images")
# 获取元数据
fruits_nuts_metadata = MetadataCatalog.get("fruits_nuts")
dataset_dicts = DatasetCatalog.get("fruits_nuts")
# 随机注释可视化
for d in random.sample(dataset_dicts, 3):
    img = cv2.imread(d["file_name"])
    visualizer = Visualizer(img[:, :, ::-1], metadata=fruits_nuts_metadata, scale=0.5)
    vis = visualizer.draw_dataset_dict(d)
    cv2.imshow("test",vis.get_image()[:, :, ::-1])
    cv2.waitKey(0)